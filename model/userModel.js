const mongoose=require('mongoose');


const userSchema=mongoose.Schema({
    email:{
        type:String,
        require:[true,'Please add an email'],
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        unique:true
    },
    name:{
        type:String,
        require:[true,'Please add a name']
    },
    password:{
        type:String,
        require:[true,'Please add a password']
    },

},    
{
    timestamps:true
})

module.exports=mongoose.model('User',userSchema)