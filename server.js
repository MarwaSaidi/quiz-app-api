const express=require('express')
const dotenv= require('dotenv').config();
const bodyParser = require('body-parser');
const {errorHandler}=require('./middleware/errorMiddleware');
const connectDB=require('./config/db')
const port=process.env.PORT || 5000;
connectDB()
const app=express();
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/api/users',require('./routes/userRoute'));
app.use(errorHandler)

app.listen(port,()=>console.log(`server started on port ${port}`));